package com.sc.app;

public class Config {
	/** 数据库设置 **/
	public static final class DATABASE {
		public static final int VERSION = 1;// 版本

		/*
		public static final String PATH = android.os.Environment
				.getExternalStorageDirectory().getAbsolutePath()
				+ "/data/data/com.sl/";// 路径
				*/
		
		/* 中间为包名，请查看AndroidManifest获取包名 */
		public static final String PATH = "/data/data/com.sc.lane/";
	}
	
	public static final String PHOTO_FOLDER = android.os.Environment
			.getExternalStorageDirectory().getAbsolutePath() + "/Lane/";
}
