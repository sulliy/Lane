package com.sc.db;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

public class MarkPoint implements Serializable{
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	int markRecordId;
	@DatabaseField
	double latitude;
	@DatabaseField
	double longitude;
	@DatabaseField
	String time;
	@DatabaseField
	String description;
	@DatabaseField
	String photos;
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("id = " + id + "\n");
		builder.append("markRecordId = " + markRecordId + "\n");
		builder.append("latitude = " + latitude + "\n");
		builder.append("longitude = " + longitude + "\n");
		builder.append("time = " + time + "\n");
		builder.append("description = " + description + "\n");
		builder.append("photos = " + photos + "\n");
		return builder.toString();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMarkRecordId() {
		return markRecordId;
	}
	public void setMarkRecordId(int markRecordId) {
		this.markRecordId = markRecordId;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhotos() {
		return photos;
	}
	public void setPhotos(String photos) {
		this.photos = photos;
	}
}
